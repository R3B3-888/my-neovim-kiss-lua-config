local core_module = {
    "core.options"
}

for _, m in ipairs(core_module) do
    local ok, err = pcall(require, m)
    if not ok then
        error("Error loading core module " .. m .. "\n\n" .. err)
    end
end
