-- IMPORTANT NOTE : This is default config, so dont change anything here.

local M = {}
M.options = {}

-- non plugin normal, available without any plugins
M.options = {
    -- NeoVim/Vim options
    clipboard = "unnamedplus",
    cmdheight = 1,
    ruler = false,
    hidden = true,
    ignorecase = true,
    smartcase = true,
    mapleader = " ",
    mouse = "a",
    number = true,
    -- relative numbers in normal mode tool at the bottom of options.lua
    numberwidth = 2,
    relativenumber = false,
    expandtab = true,
    shiftwidth = 2,
    smartindent = true,
    tabstop = 8, -- Number of spaces that a <Tab> in the file counts for
    timeoutlen = 400,
    -- interval for writing swap file to disk, also used by gitsigns
    updatetime = 250,
    undofile = true, -- keep a permanent undo (across restarts)
}

return M
