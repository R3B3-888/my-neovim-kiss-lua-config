-- :help options

local opt = vim.opt
local g = vim.g


-- local options = require("core.default_config").options

opt.autowrite = true                                    
opt.breakindent = true
opt.cmdheight = 0
opt.colorcolumn = '80'
opt.complete = '.,w,b,u,U,kspell,t'
opt.completeopt = 'menuone,preview,noinsert,noselect'
opt.cursorline = true
opt.expandtab = true                                    
opt.fillchars = { eob = " " }
opt.mouse = "a"
opt.number = true
opt.shiftwidth = 4
opt.smartindent = true
opt.smartcase = true
opt.tabstop = 4
opt.title = true
