--[[

Neovim init file
Check the repo at: https://gitlab.com/R3B3-888/my-neovim-kiss-lua-config 

--]]

local ok, err = pcall(require, "core")

if not ok then
    error("Error loading core" .. "\n\n" .. err)
end
