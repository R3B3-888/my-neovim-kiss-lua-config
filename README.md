# My Neovim config written in lua

![neovim](https://img.shields.io/badge/NeoVim-%2357A143.svg?&style=for-the-badge&logo=neovim&logoColor=white)
![lua](https://img.shields.io/badge/Lua-2C2D72?style=for-the-badge&logo=lua&logoColor=white)

## Description
This repository contains my config for neovim 0.7+ as it is written in lua.
Put the nvim folder in your `$HOME/.config`


## Visuals
_screen of my config tree_

## Installation
Install lsp server and neovim 0.7+

```bash
mv $HOME/.config/nvim $HOME/.config/nvim.bak
cp -r my-neovim-kiss-lua-config/nvim $HOME/.config/nvim/
```

## Roadmap
- [ ] init.lua
- [ ] lsp.lua
- [ ] colors.lua
- [ ] List all plugins

## Authors and acknowledgment
Inspired by [brainfucksec config](https://github.com/brainfucksec/neovim-lua)

## License
License by GPL-v3

## Project status
In progress.
